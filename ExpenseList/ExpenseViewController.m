//
//  ExpenseViewController.m
//  ExpenseList
//
//  Created by Gary Davis on 2/15/17.
//  Copyright © 2017 Gary Davis. All rights reserved.
//

#import "ExpenseViewController.h"

@interface ExpenseViewController ()

@end

@implementation ExpenseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor grayColor];
    
    expenseLocation = [UITextField new];
    expenseLocation.translatesAutoresizingMaskIntoConstraints = NO;
    expenseLocation.placeholder = @"Location";
    expenseLocation.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:expenseLocation];
    
    expenseCost = [UITextField new];
    expenseCost.placeholder = @"Cost";
    expenseCost.backgroundColor = [UIColor whiteColor];
    expenseCost.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:expenseCost];
    
    expenseNote = [UITextField new];
    expenseNote.placeholder = @"Notes";
    expenseNote.backgroundColor = [UIColor whiteColor];
    expenseNote.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:expenseNote];
    
    expenseDate = [UIDatePicker new];
    expenseDate.datePickerMode = UIDatePickerModeDate;
    expenseDate.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:expenseDate];
    
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(expenseLocation, expenseCost, expenseNote, expenseDate);
    
    NSDictionary *metrics = @{@"margin":[NSNumber numberWithInteger:20], @"topmargin":[NSNumber numberWithInteger:70]};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[expenseLocation]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[expenseCost]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[expenseNote]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[expenseDate]|" options:0 metrics:metrics views:viewsDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-topmargin-[expenseLocation(40)]-10-[expenseCost(40)]-10-[expenseNote(40)]-10-[expenseDate]" options:0 metrics:metrics views:viewsDictionary]];
    
    UIBarButtonItem* saveButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveButtonTouched:)];
    
    UIBarButtonItem* shareButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(actionButtonTouched:)];
    
    self.navigationItem.rightBarButtonItems = @[saveButton, shareButton];
    
    if (self.expense) {
        expenseLocation.text = [self.expense valueForKey:@"location"];
        expenseCost.text = [self.expense valueForKey:@"cost"];
        expenseNote.text = [self.expense valueForKey:@"notes"];
        expenseDate.date = [self.expense valueForKey:@"date"];
    }
}

-(void)actionButtonTouched:(id)sender {
    
    MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
    mailCont.mailComposeDelegate = self;
    
    [mailCont setSubject:@"location"];
    [mailCont setToRecipients:[NSArray arrayWithObject:@"grdavis101@gmail.com"]];
    NSString* message = [NSString stringWithFormat:@"%@\n%@\n%@", expenseLocation.text, expenseCost.text, expenseNote.text];
    [mailCont setMessageBody:message isHTML:NO];
    
    [self presentViewController:mailCont animated:YES completion:^{
        
    }];
}

// Then implement the delegate method
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void)saveButtonTouched:(id)sender {
    AppDelegate* appD = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = appD.persistentContainer.viewContext;
    
    if (self.expense) {
        [self.expense setValue:expenseLocation.text forKey:@"location"];
        [self.expense setValue:expenseCost.text forKey:@"cost"];
        [self.expense setValue:expenseNote.text forKey:@"notes"];
        [self.expense setValue:expenseDate.date forKey:@"date"];
    } else {
        //Create a new managed object
        NSManagedObject *newLocation = [NSEntityDescription insertNewObjectForEntityForName:@"Expense" inManagedObjectContext:context];
        [newLocation setValue:expenseLocation.text forKey:@"location"];
        [newLocation setValue:expenseCost.text forKey:@"cost"];
        [newLocation setValue:expenseNote.text forKey:@"notes"];
        [newLocation setValue:expenseDate.date forKey:@"date"];
        
    }
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
