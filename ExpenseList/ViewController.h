//
//  ViewController.h
//  ExpenseList
//
//  Created by Gary Davis on 2/15/17.
//  Copyright © 2017 Gary Davis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExpenseViewController.h"

@interface ViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
    UITableView* expenseTable;
    NSMutableArray* expenseArray;
}


@end

