//
//  ExpenseViewController.h
//  ExpenseList
//
//  Created by Gary Davis on 2/15/17.
//  Copyright © 2017 Gary Davis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import <Messages/Messages.h>
#import <MessageUI/MessageUI.h>

@interface ExpenseViewController : UIViewController<MFMailComposeViewControllerDelegate>
{
    UITextField* expenseLocation;
    UITextField* expenseCost;
    UITextField* expenseNote;
    UIDatePicker* expenseDate;
}
@property (nonatomic, strong) NSManagedObject* expense;
@end
