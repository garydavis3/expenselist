//
//  ViewController.m
//  ExpenseList
//
//  Created by Gary Davis on 2/15/17.
//  Copyright © 2017 Gary Davis. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    expenseTable = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    expenseTable.translatesAutoresizingMaskIntoConstraints = NO;
    expenseTable.delegate = self;
    expenseTable.dataSource = self;
    [self.view addSubview:expenseTable];
    
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(expenseTable);
    
    NSDictionary *metrics = @{};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[expenseTable]|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[expenseTable]|" options:0 metrics:metrics views:viewsDictionary]];
    
    UIBarButtonItem* createButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(createButtonTouched:)];
    self.navigationItem.rightBarButtonItem = createButton;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
}

-(void)loadData {
    AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Expense"];
    NSArray* array = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    expenseArray = [array mutableCopy];
    [expenseTable reloadData];
}

-(void)createButtonTouched:(id)sender {
    ExpenseViewController* dvc = [ExpenseViewController new];
    [self.navigationController pushViewController:dvc animated:YES];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSManagedObject* object = expenseArray[indexPath.row];
    
    cell.textLabel.text = [object valueForKey:@"location"];
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return expenseArray.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ExpenseViewController* dvc = [ExpenseViewController new];
    dvc.expense = expenseArray[indexPath.row];
    [self.navigationController pushViewController:dvc animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
